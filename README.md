# Micro-frontends-universal-rendering-ssr

Angular Universal & Server-side rendering. (SSR). Tipically is make in the client side (CSR).

## Fuentes:

Micro-servicios con Angular (SSR):
- https://github.com/matt-gold/single-spa-angular-cli

Micro-servicios con Angular (CSR):
- https://github.com/manfredsteyer/angular-microfrontend

Micro-servicios con Javascript (HTML Components):
- https://github.com/neuland/micro-frontends
	
Python-Angular-Docker	
- https://gitlab.com/seanwasere/Seans-Angular6-Python-Flask-Boilerplate

CI/CD with Gitlab Runner and Docker-Compose
- https://youtu.be/RV0845KmsNI

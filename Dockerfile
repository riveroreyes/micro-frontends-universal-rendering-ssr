FROM alpine:3.9
LABEL Carlos Rivero  <crivero@ansib.com>

RUN apk update
RUN apk add --update nodejs npm
RUN apk add nginx
RUN set -x ; \
  addgroup -g 82 -S www-data ; \
  adduser -u 82 -D -S -G www-data www-data && exit 0 ; exit 1

#Main container
COPY ./AngularApp /AngularApp
WORKDIR /AngularApp
RUN npm install
RUN node --max_old_space_size=1024 node_modules/@angular/cli/bin/ng build --prod --crossOrigin=anonymous  --deploy-url http://localhost:4200/ 

#First child
COPY ./child1 /child1
WORKDIR /child1
RUN npm install
RUN node --max_old_space_size=1024 node_modules/@angular/cli/bin/ng build --prod --crossOrigin=anonymous  --deploy-url http://localhost:4201/
RUN cp dist/child1/main-es5.* dist/child1/main.js

#Second child
COPY ./child2 /child2
WORKDIR /child2
RUN npm install
RUN node --max_old_space_size=1024 node_modules/@angular/cli/bin/ng build --prod --crossOrigin=anonymous  --deploy-url http://localhost:4202/
RUN cp dist/child2/main-es5.* dist/child2/main.js

COPY ./nginx.conf	    /etc/nginx/nginx.conf


#third child
COPY ./child3 /child3
WORKDIR /child3
RUN npm install
RUN node --max_old_space_size=1024 node_modules/@angular/cli/bin/ng build --prod --crossOrigin=anonymous  --deploy-url http://localhost:4203/
RUN cp dist/child3/main-es5.* dist/child3/main.js

COPY ./nginx.conf	    /etc/nginx/nginx.conf



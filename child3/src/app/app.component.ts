import { Component } from '@angular/core';

@Component({
  selector: 'app-child3-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'child3';
}

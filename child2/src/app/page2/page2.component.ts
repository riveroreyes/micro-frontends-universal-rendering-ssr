import { Component, OnInit } from '@angular/core';
import { PagesService } from '../pages.service';

@Component({
  selector: 'app-child2-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component implements OnInit {

  constructor(private pagesService: PagesService) { }

  ngOnInit() {
    // this.pagesService.stateChanged.next(false);
  }

}

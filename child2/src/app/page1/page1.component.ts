import { Component, OnInit } from '@angular/core';
import { PagesService } from '../pages.service';

@Component({
  selector: 'app-child2-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {
  id: string;
  name: string;
  params: object;
  constructor(private pagesService: PagesService) { }

  ngOnInit() {
    // this.pagesService.stateChanged.next(false);
    const domElement = document.getElementById('container-div');  // const domElement = document.querySelector('#container-div);
    const params = domElement.getAttribute('data-dir'); // o domElement.dataset.dir;
    console.log('DESDE', params);
    if (params !== undefined) {
      this.params = this.queryStringToObject(params);
      console.log('LUEGO', this.params);
      let key = 'id';
      if (this.params[key]) {
        this.id = this.params[key];
      }
      key = 'name';
      if (this.params[key]) {
        this.name = this.params[key];
      }
    }

  }

  queryStringToObject = (queryString): object => {
    const obj = {};
    if (queryString) {
      decodeURI(queryString).split('&').map((item) => {
        const [k, v] = item.split('=');
        v ? obj[k] = v : null;
      });
    }
    return obj;
  }

}

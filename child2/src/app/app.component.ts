import { Component, OnInit, OnDestroy } from '@angular/core';
import { PagesService } from './pages.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-child2-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  subs: Subscription;
  title = 'child2';
  start = true;

  constructor(private pagesService: PagesService){}

  ngOnInit() {
    this.subs = this.pagesService.stateChanged.subscribe( (state) => this.start = state  );
  }

  onChangePage() {
    this.pagesService.stateChanged.next(false);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}

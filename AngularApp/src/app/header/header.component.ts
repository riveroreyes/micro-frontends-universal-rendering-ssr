import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../config/config-service.service';
import { Menu } from '../models/menu.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  menus: Menu[];
  constructor(private configService: ConfigService) { }

  ngOnInit() {
    // this.menus = this.configService.menus;
    this.menus = [
      {
        link: '/',
        name: 'Inicio'
      },
      {
        link: 'pages/child1',
        name: 'Recargas y Servicios'
      },
      {
        link: 'pages/child2',
        name: 'Ventas'
      },
      {
        link: 'pages/child3',
        name: 'Administración'
      },
      {
        link: 'pages/profile',
        name: 'Configuración'
      },
      {
        link: 'pages/oferts',
        name: 'Ofertas'
      }
    ];
  }

}

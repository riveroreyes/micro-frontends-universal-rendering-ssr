// ..ojo..spa/main
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { SingleSpaService } from '../../services/single-spa.service';

@Component({
  selector: 'app-spa-host',
  template: '<div id="container-div" #appContainer></div>'
})
export class SpaHostComponent implements OnInit, OnDestroy {

  constructor(private service: SingleSpaService, private route: ActivatedRoute) { }

  @ViewChild('appContainer', { static: true }) private appContainerRef: ElementRef;
  private appName: string;
  private params: object;

  ngOnInit() {
    this.appName = this.route.snapshot.data.app;
    console.log('APP-CONTAINER', this.appContainerRef.nativeElement);

    this.route.params.subscribe(
      (params: Params) => {
        console.log('PARAMS',params);

        this.params = params;
        console.log('ID', this.params);

      }
    );

    this.service.mount(this.appName, this.appContainerRef.nativeElement, this.params).subscribe();
  }

  async ngOnDestroy() {
    await this.service.unmount(this.appName).toPromise();
  }
}

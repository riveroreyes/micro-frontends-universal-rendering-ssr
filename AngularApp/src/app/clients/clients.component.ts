import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.addEventListener('customEvent', this.refresh);
  }

  refresh() {
    alert('evento recibido');
  }

  go(){
    alert('go');
  }
}

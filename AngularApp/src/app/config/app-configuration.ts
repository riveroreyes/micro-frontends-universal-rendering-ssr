export abstract class AppConfiguration {
  username: string;
  roles: string[];
}

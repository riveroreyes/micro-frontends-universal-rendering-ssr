import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Menu } from '../models/menu.model';

@Injectable()
export class ConfigService {
  private _configData: any;
  private _promise: Promise<any>;
  private _promiseDone = false;

  private _menus: Menu[];

  constructor(private http: HttpClient) { }

  loadConfig(): Promise<any> {
    // const url = 'app.config.json';
    const url = 'https://react-my-burger-91edd.firebaseio.com/appconfig.json';
    this._configData = null;

    if (this._promiseDone) {
      return Promise.resolve();
    }

    if (this._promise != null) {
      return this._promise;
    }

    this._promise = this.http
      .get(url, { headers: new HttpHeaders() })
      .map((res: Response) => res)
      .toPromise()
      .then((data: any) => {
        this._configData = data; this._promiseDone = true;
      })
      .catch((err: any) => { this._promiseDone = true; return Promise.resolve(); });
    return this._promise;
  }

  get configData(): any {
    return this._configData;
  }

  validatedRole(roles: string[]): boolean {
    return roles.length === 0 || roles.map(x => this.configData.username.roles.includes(x)).filter(x => x).length > 0;
  }

  get menus() {
    return this.configData.menus.filter(x => {
      const path = this.configData.mainNavigation.filter(y => x.link === '/' + y.path)[0];
      if (path.data && path.data.roles) {
        return this.validatedRole(path.data.roles);
      } else {
        return true;
      }
    });
  }

}

// ..ojo..spa/main
import { NgModule, APP_INITIALIZER, Injector } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { SpaHostComponent } from './spa-host/spa-host.component';
import { HomeComponent } from './home/home.component';

import { AppConfiguration } from './config/app-configuration';
import { ConfigService } from './config/config-service.service';
import { ClientsComponent } from './clients/clients.component';
import { stringify } from 'querystring';

const appRoutes: Routes = [];
const components = [{
  spaHostComponent: SpaHostComponent,
  homeComponent: HomeComponent,
  clientsComponent: ClientsComponent,
}];
const entryComp = [SpaHostComponent, HomeComponent, ClientsComponent];

export function configServiceFactory(injector: Injector, configService: ConfigService): any {
  return () => {
    return configService
      .loadConfig()
      .then(res => {
        // res is always null
        const customRoutes: Routes = [];

        configService.configData.mainNavigation.map(x => {
          const el: object = {};
          let key = 'path';
          el[key] = x.path;
          if (x.children) {
            key = 'children';
            el[key] = [{
              path: x.children[0].path,
              component: components[0][x.children[0].component],
              data: { app: x.children[0].data.app }
            }];
          }
          if (x.pathMatch) {
            key = 'pathMatch';
            el[key] = x.pathMatch;
          }
          if (x.component) {
            key = 'component';
            el[key] = components[0][x.component];
          }
          key = 'data';
          el[key] = { roles: [] };
          if (x.data && x.data.roles) {
            el[key] = { roles: x.data.roles };
          }
          customRoutes.push(el);
        });

        let filteredRoutes = customRoutes.filter(item => {
          if (!item.data.roles || item.data.roles.length === 0) {
            return true;
          }
          return item.data.roles.filter(role => {
            return configService.configData.username.roles.includes(role);
          }).length > 0;
        });

        filteredRoutes = [
          {
            path: 'pages/child1',
            children: [
              {
                path: '**',
                component: SpaHostComponent,
                data: { app: 'child1' }
              }
            ],
            data: { roles: ['client', 'admin'] }
          },
          {
            path: 'pages/child2',
            children: [
              {
                path: ':id/:name',
                component: SpaHostComponent,
                data: { app: 'child2' }
              },
              {
                path: ':id',
                component: SpaHostComponent,
                data: { app: 'child2' }
              },
              {
                path: '',
                component: SpaHostComponent,
                data: { app: 'child2' }
              }
            ],
            data: { roles: ['client', 'admin'] }
          },
          {
            path: 'pages/child3',
            children: [
              {
                path: '**',
                component: SpaHostComponent,
                data: { app: 'child3' }
              }
            ],
            data: { roles: ['provider', 'admin'] }
          },
          {
            path: 'pages/profile',
            children: [
              {
                path: '**',
                component: SpaHostComponent,
                data: { app: 'profile' }
              }
            ],
            data: { roles: ['admin'] }
          },
          {
            path: 'oferts',
            children: [
              {
                path: '**',
                component: SpaHostComponent,
                data: { app: 'oferts' }
              }
            ],
            data: { roles: ['admin'] }
          },
          {
            path: 'clients',
            component: ClientsComponent,
            data: { roles: [] }
          },
          {
            path: '',
            component: HomeComponent,
            pathMatch: 'full',
            data: { roles: [] }
          }
        ];


        const router: Router = injector.get(Router);
        router.resetConfig(filteredRoutes);
      });
  }
}


@NgModule({
  imports: [RouterModule.forRoot(appRoutes), HttpClientModule],
  exports: [RouterModule],
  providers: [
    ConfigService,
    {
      provide: AppConfiguration,
      deps: [HttpClient],
      useExisting: ConfigService
    },
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [Injector, ConfigService],
      multi: true
    },
  ],
  entryComponents: entryComp,
})
export class AppRoutingModule { }

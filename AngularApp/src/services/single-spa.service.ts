// ..ojo..spa/main
import { Injectable } from '@angular/core'
import { Parcel, mountRootParcel, } from 'single-spa'
import { Observable, from } from 'rxjs'
import { mapTo, tap } from 'rxjs/operators'
import { stringify } from 'querystring'

@Injectable({
  providedIn: 'root'
})
export class SingleSpaService {
  private loadedParcels: {
    [appName: string]: Parcel
  } = {}

  constructor() { }

  mount(appName: string, domElement: HTMLElement, customProp1: object): Observable<void> {
    console.log('WINDOW', domElement)

    return from(window.System.import(appName))
      .pipe(
        tap(app => {
          console.log('APP', customProp1)

          // tslint:disable-next-line: object-literal-shorthand
          const parcelProps = { domElement, customProp1: customProp1 };
          this.loadedParcels[appName] = mountRootParcel(app, parcelProps);
          domElement.setAttribute('data-dir', stringify( customProp1 ) );
          console.log('ATRINU', domElement);
        }),
        mapTo(null)
      )
  }

  unmount(appName: string): Observable<void> {
    return from(this.loadedParcels[appName].unmount()).pipe(
      tap(() => delete this.loadedParcels[appName]),
      mapTo(null)
    )
  }
}

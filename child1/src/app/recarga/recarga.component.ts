import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'child1-recarga',
  templateUrl: './recarga.component.html',
  styleUrls: ['./recarga.component.css']
})
export class RecargaComponent implements OnInit {
  chargedForm: FormGroup;
  name: string;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => this.name = params['name']);
    this.chargedForm = new FormGroup({
      phone: new FormControl(''),
      operator: new FormControl('')
    });
  }

  onSubmit() {
    // this.router.navigate(['.'], { queryParamsHandling: 'merge', queryParams: { id: 17 }});
    console.log('SEARCH');
    // window.dispatchEvent(new CustomEvent('client-message', { detail: this.name+' | Telefono: ' + this.chargedForm.value.phone + '  |  Operadora: ' + this.chargedForm.value.operator }));
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { createCustomElement } from '@angular/elements';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { RecargaComponent } from './recarga/recarga.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClientAWidgetComponent } from './client-a-widget/client-a-widget.component';
import { ButtonComponent } from './button/button.component';
import { ConfigurationService} from './configuration.service';

@NgModule({
  declarations: [
    AppComponent,
    EmptyRouteComponent,
    RecargaComponent,
    ClientAWidgetComponent,
    ButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ConfigurationService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [
    ClientAWidgetComponent,
    ButtonComponent
  ]
})
export class AppModule {

  constructor(private injector: Injector) {
    const selector = 'client-a-widget';
    const myElementExists = !!customElements.get(selector);
    if (!myElementExists) {
      const widgetElement = createCustomElement(ClientAWidgetComponent, { injector: this.injector });
      customElements.define(selector, widgetElement);
    }
  }

}

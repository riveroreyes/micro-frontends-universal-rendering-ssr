import { Component, Input, Output, EventEmitter, ViewEncapsulation, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  //selector: 'client-a-widget',
  template: `
  <div id="widget">
  <div class="card">
    <div class="header">
      <h1>{{ title }}</h1>
      <h3 class="text-danger">{{ message.length === 0 ? '' : message | titlecase}}</h3>
      <button (click)="handleClick()" class="btn btn-success">{{ label }}</button>
    </div>
    <div class="content">
  <table class="table table-contensed table-striped">
      <thead>
      <tr>
          <th>Tipo</th>
          <th>Desde</th>
          <th>Hasta</th>
      </tr>
      </thead>
      <tr>
          <td>Admin</td>
          <td>01/01/2020</td>
          <td>31/12/2020</td>
      </tr>
      <tr>
          <td>Provider</td>
          <td>01/01/2020</td>
          <td>31/12/2020</td>
      </tr>

      </table>
  </div>
</div>
</div>


  `,
  styles: [`
        #widget { padding:10px; border: 2px darkred dashed }
  `],
  encapsulation: ViewEncapsulation.Emulated
})
export class ClientAWidgetComponent implements OnInit {
  control = new FormControl();
  value$: Observable<string>;
  @Input() label: string;
  @Input() title: string;
  @Input() message = '';
  @Output() action = new EventEmitter<number>();
  private numberOfClicks = 0;

  ngOnInit(): void {
    this.control.valueChanges.subscribe(x => console.log(x));
    this.value$ = this.control.valueChanges;
  }

  clickMe(): void {
    console.log('ouch!');
  }

  handleClick() {
    this.numberOfClicks++;
    this.action.emit(this.numberOfClicks);
    console.log(this.numberOfClicks);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmptyRouteComponent } from './empty-route/empty-route.component';
import { RecargaComponent } from './recarga/recarga.component';


const routes: Routes = [
  { path: 'pages/child1/recarga/:name', component: RecargaComponent },
  {
    path: '**',
    component: EmptyRouteComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

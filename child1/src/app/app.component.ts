import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ConfigurationService } from './configuration.service';

@Component({
  selector: 'child1-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'child1';
  num = 0;
  message = '';
  linea = '';

  constructor(private confService: ConfigurationService) {
    console.log('CREANDO APP DE RECARGAS Y SERVICIOS');
    window.addEventListener('client-message', (e: CustomEvent) => {
      console.log('ESCUCHANDO EN RECARGAS Y SERVICIOS');
      console.log(e.detail);
      this.confService.messageChanged.next(e.detail);
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit(){
    this.confService.messageChanged.subscribe(
      (message: string) => {
        this.message = message;
        this.linea = message.split('|')[0];
      }
    );
  }

  onButtonEmit(data: CustomEvent) {
    console.log(data.detail);
    this.num = data.detail;
  }
}

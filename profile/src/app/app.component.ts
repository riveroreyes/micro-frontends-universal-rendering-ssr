import { Component, Input, OnInit } from '@angular/core';
import { ConfigurationService } from './configuration.service';

@Component({
  selector: 'profile-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @Input() data: string;
  title = 'Configuraciones ';
  num = 0;
  message = '';


  constructor(private confService: ConfigurationService){
    console.log('CREANDO APP DE CONFIGURACION');
    window.addEventListener('client-message', (e: CustomEvent) => {
      console.log('ESCUCHANDO EN CONFIGURACION');
      console.log(e.detail);
      this.confService.messageChanged.next(e.detail);
    } );
  }

  ngOnInit(){
    this.confService.messageChanged.subscribe(
      (message: string) => this.message = message
    );
  }

  onButtonEmit(data: CustomEvent){
    console.log(data.detail);
    this.num = data.detail;
  }
}
